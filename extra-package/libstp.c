#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include "/usr/lib64/libstp/libstp-c.h"

int main(int argc, char *argv[]) {
    if (argc == 2 && strcmp(argv[1], "--version") == 0 || argc == 2 && strcmp(argv[1], "-v") == 0) {
        program_version("libstp, libstp-extras");
    } else if (argc == 2 && strcmp(argv[1], "--help") == 0 || argc == 2 && strcmp(argv[1], "-h") == 0) {
        runCommand("man libstp");
    }
}
