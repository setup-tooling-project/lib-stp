#include "libstp-c.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdarg.h>
#include <termios.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <sys/wait.h>
#include <utmp.h>

static const char* current_config_file = NULL;

char *strlwr(char *str) {
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'A' && str[i] <= 'Z') {
            str[i] += 32; // Add 32 to convert uppercase to lowercase
        }
    }
    return str;
}

int runCommand(const char* format, ...) {
    char newRun[2048];
    va_list args;
    va_start(args, format);

    vsnprintf(newRun, sizeof(newRun), format, args);
    va_end(args);

    int result = system(newRun);
    return result;
}

bool commandExists(const char* command) {                                                       // returns a value after checking if a program exists
    FILE* fp = popen(command, "r");
    if (fp == NULL) {
        return false;
        pclose(fp);
    } else {
        return true;
        pclose(fp);
    }
}

bool installProgram(const char *command) {
    printf("Program not found. Install with 'sudo dnf install %s'? (Y/n): ", command);

    char answer = getchar();  // Get user input (y or n)
    getchar();                // Consume newline character

    if (answer == 'y' || answer == 'Y' || answer == '\n') {
        char installCommand[100];
        sprintf(installCommand, "sudo dnf install %s", command);
        int result = system(installCommand);
        if (result == 0) {
            printf("Installation successful!\n");
            return true;
        } else {
            printf("Installation failed.\n");
            return false;
        }
    } else {
        return false; // User chose not to install
    }
}

int getMenuOption(int maxOption) {                                                              // set menu limits
    int option;
    scanf("%d", &option);
    while (option < 1 || option > maxOption) {
        printf("Invalid option. Please try again.\n");
        scanf("%d", &option);
    }
    return option;
}

void extractOSVersion(char *version) {
    FILE *file = fopen("/etc/os-release", "r");
    if (file == NULL) {
        perror("Could not open /etc/os-release");
        exit(EXIT_FAILURE);
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        if (strncmp(line, "VERSION_ID=", 11) == 0) {
            // Extracting the version information including additional characters
            strcpy(version, line + 11);
            char *newline = strchr(version, '\n');
            if (newline != NULL)
                *newline = '\0';

            // Finding the first parenthesis, if present, and removing everything after it
            char *parenthesis = strchr(version, '(');
            if (parenthesis != NULL)
                *parenthesis = '\0';

            // Remove the dot and what comes after it from the version number
            int j = 0;
            for (size_t i = 0; i < strlen(version); i++) { // Changed int to size_t
                if (version[i] == '.') {
                    break;
                }
                version[j] = version[i];
                j++;
            }
            version[j] = '\0'; // Null-terminate the processed version

            // Removing any trailing spaces
            int len = strlen(version);
            while (len > 0 && isspace(version[len - 1])) {
                version[--len] = '\0';
            }

            fclose(file);
            return;
        }
    }

    fclose(file);
    // If VERSION= line not found, indicate failure in the version string
    strcpy(version, "Version not found");
}

void extractLinuxDistro(char *distro) {
    FILE *file = fopen("/etc/os-release", "r");
    if (file == NULL) {
        perror("Could not open /etc/os-release");
        exit(EXIT_FAILURE);
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        if (strncmp(line, "ID=", 3) == 0) {
            strcpy(distro, line + 3); // Copy the content after "ID="
            char *newline = strchr(distro, '\n');
            if (newline != NULL) {
                *newline = '\0'; // Remove the newline character
            }

            // Remove quotation marks (if present)
            if (distro[0] == '\"' && distro[strlen(distro) - 1] == '\"') {
                distro[strlen(distro) - 1] = '\0'; // Remove the last quotation mark
                memmove(distro, distro + 1, strlen(distro)); // Shift the string to the left
            }

            fclose(file);
            return;
        }
    }

    fclose(file);
    strcpy(distro, "Distro not found");
}

bool installWithPackageManager(const char* packageName, const char* packageManager) {           // running an installation and giving back the result of that
    char command[256];
    if (strcmp(packageManager, "sudo snap") == 0 ||
        strcmp(packageManager, "snap") == 0) {
        snprintf(command, sizeof(command), "%s install %s", packageManager, packageName);
    } else {
        snprintf(command, sizeof(command), "%s install -y %s", packageManager, packageName);
    }
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool uninstallWithFlatpak(const char* packageName) {                                           // uninstalling a program with Flatpak and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "flatpak uninstall -y %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool uninstallWithDNF(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo dnf remove -y %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool uninstallWithRO(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo rpm-ostree uninstall -y %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool uninstallWithDNF5(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo dnf5 remove -y %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool uninstallWithSnap(const char* packageName) {                                               // uninstalling a program with snapd and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo snap remove %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool searchWithFlatpak(const char* packageName) {                                           // uninstalling a program with Flatpak and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "flatpak search %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool searchWithDNF(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo dnf search --refresh %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool searchWithRO(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo rpm-ostree search %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool searchWithDNF5(const char* packageName) {                                                // uninstalling a program with DNF and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo dnf5 search --refresh %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool searchWithSnap(const char* packageName) {                                               // uninstalling a program with snapd and giving back the result of that
    char command[256];
    snprintf(command, sizeof(command), "sudo snap find %s", packageName);
    printf("Executing command: %s\n", command);
    int result = system(command);
    return (result == 0);
}

bool isValidPackageName(const char *packageName) {
    // Implement strict validation here.
    // Example: Allow only alphanumeric, hyphen, underscore, period:
    for (int i = 0; packageName[i] != '\0'; i++) {
        if (!isalnum(packageName[i]) && packageName[i] != '_' && packageName[i] != '-' && packageName[i] != '.') {
            return false;
        }
    }
    return true;
}

int checkProgramExists(const char* programName) {
    char fullPath[100];
    sprintf(fullPath, "%s%s", PROGRAMS_PATH, programName);

    if (access(fullPath, F_OK) != -1) {
        return 1;  // Program exists
    } else {
        return 0;  // Program does not exist
    }
}

int shouldShowNotice_tp() {
    FILE *file = fopen(getConfigFile(), "r");
    if (file != NULL) {
        char line[256];
        while (fgets(line, sizeof(line), file) != NULL) {
            // Trim the newline if present
            line[strcspn(line, "\n")] = '\0';

            // Skip comments
            if (line[0] == '#') {
                continue;  // Move to the next line
            }
            if (strstr(line, "tp-notice = ") != NULL) {
                char* response = strchr(line, '=') + 2;
                response[strcspn(response, "\n")] = '\0';  // Remove newline character
                if (strcasecmp(response, "True") == 0) {
                    return 1;  // Show the notice
                } else if (strcasecmp(response, "False") == 0) {
                    return 0;  // Don't show the notice
                }
            }
        }
    }
    fclose(file);
    return 1;  // Show the notice by default
}

int shouldShowNotice_beta() {
    FILE *file = fopen(getConfigFile(), "r");
    if (file == NULL) {
        return 1;  // Show the notice by default if can't open file
    }

    char line[256];
    while (fgets(line, sizeof(line), file) != NULL) {
        // Trim the newline if present
        line[strcspn(line, "\n")] = '\0';

        // Skip comments
        if (line[0] == '#') {
            continue;  // Move to the next line
        }
        if (strstr(line, "beta-notice = ") != NULL) {
            char* response = strchr(line, '=') + 2;
            response[strcspn(response, "\n")] = '\0';  // Remove newline character
            if (strcasecmp(response, "True") == 0) {
                fclose(file);  // Close file before returning
                return 1;  // Show the notice
            } else if (strcasecmp(response, "False") == 0) {
                fclose(file);  // Close file before returning
                return 0;  // Don't show the notice
            }
        }
    }

    fclose(file);  // Close file at the end
    return 1;  // Show the notice by default
}

void autostart_beta() {
    FILE *file = fopen(getConfigFile(), "r");
    if (file == NULL) {
        printf("Error opening configuration file. Autostart will be skipped.\n");
        return;
    }

    char line[256]; // Use a larger buffer to be safe

    // Iterate through all lines in the file
    while (fgets(line, sizeof(line), file) != NULL) {
        // Trim the newline if present
        line[strcspn(line, "\n")] = '\0';

        // Skip comments
        if (line[0] == '#') {
            continue;  // Move to the next line
        }

        if (strstr(line, "autostart = ") != NULL) {
            char* response = strchr(line, '=') + 2;

            int autostartValue = strtol(response, NULL, 10);

            /* printf("Found autostart line: %s\n", line);
             *          printf("Response value: %s\n", response);
             *          printf("Autostart Value: %d\n", autostartValue); */

            if (autostartValue >= 0 && autostartValue <= 3) {
                switch (autostartValue) {
                    case CLI:  {
                        char fullPath[100];
                        sprintf(fullPath, "%s%s", PROGRAMS_PATH, "setup-tool-cli-beta");
                        execlp(fullPath, "setup-tool-cli-beta", NULL); // Launch CLI
                    } break;

                    case TUI:  {
                        char fullPath[100];
                        sprintf(fullPath, "%s%s", PROGRAMS_PATH, "setup-tool-tui-beta");
                        execlp(fullPath, "setup-tool-tui-beta", NULL); // Launch TUI
                    } break;

                    case GUI:  {
                        char fullPath[100];
                        sprintf(fullPath, "%s%s", PROGRAMS_PATH, "setup-tool-gui-beta");
                        execlp(fullPath, "setup-tool-gui-beta", NULL); // Launch GUI
                    } break;

                    default:
                        break; // Handle potentially invalid autostart values
                }
            }
            break; // We found the setting, no need to continue
        }
    }

    fclose(file);
}

void saveUserResponse_tp(char response) {
    const char* configResponse = (response == 'Y' || response == 'y') ? "False" : "True";
    FILE *file = fopen(getConfigFile(), "r+"); // Open for reading and writing

    if (file != NULL) {
        char line[100]; // Adjust the size if lines are longer than 100 characters
        FILE *tmpFile = tmpfile(); // Create a temporary file

        if (tmpFile == NULL) {
            fprintf(stderr, "Error creating temporary file.\n");
            fclose(file);
            return;
        }

        // Copy lines, updating the notice if found
        while (fgets(line, sizeof(line), file) != NULL) {
            if (strstr(line, "tp-notice = ") != NULL) {
                fprintf(tmpFile, "tp-notice = %s\n", configResponse);
            } else {
                fputs(line, tmpFile);
            }
        }

        // Overwrite original file with the modified content
        rewind(file);
        rewind(tmpFile);
        while (fgets(line, sizeof(line), tmpFile) != NULL) {
            fputs(line, file);
        }

        fclose(tmpFile);
        fclose(file);
    }
}

char* getSecurePasswordSTP() {
    struct termios oldTerm, newTerm;
    char *password_stp = NULL;

    if (tcgetattr(STDIN_FILENO, &oldTerm) == -1) {
        perror("Error getting terminal attributes");
        exit(1);
    }

    newTerm = oldTerm;
    newTerm.c_lflag &= ~(ECHO | ECHONL); // Disable echo and newline echo

    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &newTerm) == -1) {
        perror("Error setting terminal attributes");
        exit(1);
    }

    password_stp = getpass("Please enter your sudo password: ");

    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &oldTerm) == -1) {
        perror("Error restoring terminal attributes");
        exit(1);
    }

    return password_stp; // Caller is responsible for freeing memory
}

char* getVersion(const char* programName) {
    FILE *fp;
    char command[100];  // Adjust size if needed
    char version_stp[50];

    // Construct the rpm command using the program name
    snprintf(command, sizeof(command), "rpm -q %s", programName);

    fp = popen(command, "r");
    if (fp == NULL) {
        printf("Failed to run command\n");
        exit(1);
    }

    fgets(version_stp, sizeof(version_stp), fp);
    pclose(fp);

    if (version_stp[strlen(version_stp) - 1] == '\n') {
        version_stp[strlen(version_stp) - 1] = '\0';
    }

    return strdup(version_stp);
}

void set_log_path() {
    char* log_path = NULL;
    FILE *file = fopen(getConfigFile(), "r");
    if (file == NULL) {
        fprintf(stderr, "Error opening config file: %s\n", getConfigFile());
        log_path = strdup("default_log_path.txt");
        return;
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, file)) != -1) {
        // Trim the newline if present
        if (line[read - 1] == '\n') {
            line[read - 1] = '\0';
        }

        // Skip comments
        if (line[0] == '#') {
            continue;
        }

        if (strstr(line, "log_location = ") != NULL) {
            char* new_log_path = strchr(line, '=') + 2;
            new_log_path[strcspn(new_log_path, "\n")] = '\0';

            if (log_path != NULL) {
                free(log_path);
            }

            log_path = strdup(new_log_path);
            break;
        }
    }

    free(line); // Free the memory allocated by getline
    fclose(file);

    if (log_path == NULL) {
        log_path = strdup("default_log_path.txt");
    }
}

const char* getScriptShell(const char* path, const char* extension) {
    // First check shebang line
    FILE *file = fopen(path, "r");
    if (file) {
        char firstLine[256];
        if (fgets(firstLine, sizeof(firstLine), file) != NULL) {
            fclose(file);
            if (strncmp(firstLine, "#!", 2) == 0) {
                // Return the shell path from shebang
                // Strip newline if present
                char *newline = strchr(firstLine + 2, '\n');
                if (newline) *newline = '\0';
                // Skip spaces after #!
                char *shell = firstLine + 2;
                while (*shell == ' ') shell++;
                // Allocate and return the shell path
                return strdup(shell);
            }
        } else {
            fclose(file);
        }
    }

    // If no shebang or can't read file, use extension
    if (strcasecmp(extension, ".sh") == 0) {
        // For .sh, try to detect preferred system shell
        if (access("/bin/bash", X_OK) == 0) return "/bin/bash";
        if (access("/bin/dash", X_OK) == 0) return "/bin/dash";
        return "/bin/sh";
    }
    else if (strcasecmp(extension, ".bash") == 0) return "/bin/bash";
    else if (strcasecmp(extension, ".zsh") == 0) return "/bin/zsh";
    else if (strcasecmp(extension, ".dash") == 0) return "/bin/dash";
    else if (strcasecmp(extension, ".ksh") == 0) return "/bin/ksh";
    else if (strcasecmp(extension, ".mksh") == 0) return "/bin/mksh";
    else if (strcasecmp(extension, ".fish") == 0) return "/bin/fish";
    else if (strcasecmp(extension, ".c") == 0) return NULL; // Handle C files differently

    return NULL;
}

int compile_and_execute_c(const char* source_path) {
    char temp_template[] = "/tmp/script_XXXXXX";
    char *temp_dir = mkdtemp(temp_template);
    if (!temp_dir) {
        return SCRIPT_EXECUTION_ERROR;
    }

    // Create paths for executable
    char obj_path[PATH_MAX];
    snprintf(obj_path, sizeof(obj_path), "%s/program", temp_dir);

    // Construct compilation command with proper security flags
    char compile_cmd[PATH_MAX * 3];
    snprintf(compile_cmd, sizeof(compile_cmd),
            "gcc -O2 -I/usr/lib64/libstp -L/usr/lib64/libstp /usr/lib64/libstp/libstp-c.so -o \"%s\" \"%s\" -Wall -Wextra -Werror -fstack-protector-strong -D_FORTIFY_SOURCE=2",
            obj_path, source_path);

    // Compile the program
    int compile_result = system(compile_cmd);
    if (compile_result != 0) {
        fprintf(stderr, "Compilation failed\n");
        rmdir(temp_dir);
        return SCRIPT_EXECUTION_ERROR;
    }

    // Check if compilation succeeded and executable exists
    if (access(obj_path, X_OK) != 0) {
        fprintf(stderr, "Compiled program not executable\n");
        unlink(obj_path);
        rmdir(temp_dir);
        return SCRIPT_EXECUTION_ERROR;
    }

    // Execute the compiled program
    pid_t pid = fork();
    if (pid == -1) {
        unlink(obj_path);
        rmdir(temp_dir);
        return SCRIPT_EXECUTION_ERROR;
    }

    if (pid == 0) {  // Child process
        // Set up restricted environment for execution
        char *const envp[] = {
            "PATH=/usr/local/bin:/usr/bin:/bin",
            NULL
        };

        execle(obj_path, obj_path, NULL, envp);
        _exit(127);  // execle failed
    }

    // Parent process
    int status;
    waitpid(pid, &status, 0);

    // Clean up
    unlink(obj_path);
    rmdir(temp_dir);

    return WIFEXITED(status) ? WEXITSTATUS(status) : SCRIPT_EXECUTION_ERROR;
}

int listScripts(const char* directory, Script scripts[], int current_count, int max_scripts) {
    DIR *dir;
    struct dirent *entry;
    int scriptCount = 0;
    struct stat st;

    if (!directory || !scripts || current_count < 0 || max_scripts <= 0) {
        errno = EINVAL;
        return SCRIPT_PATH_ERROR;
    }

    if (current_count >= max_scripts) {
        return SCRIPT_BOUNDS_ERROR;
    }

    dir = opendir(directory);
    if (dir == NULL) {
        return SCRIPT_DIR_ERROR;
    }

    char full_path[MAX_PATH];
    const char *valid_extensions[] = {
        ".sh", ".bash", ".zsh", ".dash", ".ksh",
        ".mksh", ".fish", ".c", NULL
    };

    while ((entry = readdir(dir)) != NULL &&
           (current_count + scriptCount) < max_scripts) {
        // Skip . and ..
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Build full path
        int path_len = snprintf(full_path, sizeof(full_path), "%s/%s", directory, entry->d_name);
        if (path_len < 0 || (size_t)path_len >= sizeof(full_path)) {
            continue;  // Path too long, skip
        }

        // Get file status
        if (stat(full_path, &st) != 0) {
            continue;  // Can't stat file, skip
        }

        // Check if it's a regular file
        if (!S_ISREG(st.st_mode)) {
            continue;
        }

        // Check extension
        char *extension = strrchr(entry->d_name, '.');
        if (!extension) {
            continue;
        }

        bool valid_ext = false;
        for (const char **ext = valid_extensions; *ext != NULL; ext++) {
            if (strcasecmp(extension, *ext) == 0) {
                valid_ext = true;
                break;
            }
        }

        if (!valid_ext) {
            continue;
        }

        // Store script information
        Script *current = &scripts[current_count + scriptCount];

        if (strlcpy(current->name, entry->d_name, sizeof(current->name)) >= sizeof(current->name)) {
            // Name too long, skip
            continue;
        }

        if (strlcpy(current->path, full_path, sizeof(current->path)) >= sizeof(current->path)) {
            // Path too long, skip
            continue;
        }

        current->permissions = st.st_mode;
        current->modified = st.st_mtime;

        scriptCount++;
    }

    closedir(dir);
    return scriptCount;
}

void displayScriptMenu(Script scripts[], int scriptCount) {
    if (!scripts || scriptCount <= 0) {
        printf("No scripts available.\n");
        return;
    }

    printf("\nAvailable scripts:\n");
    printf("%-4s %-30s %-10s %-20s\n", "No.", "Name", "Executable", "Last Modified");
    printf("------------------------------------------------------------\n");

    for (int i = 0; i < scriptCount; i++) {
        char timestamp[20];
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M",
                localtime(&scripts[i].modified));

        printf("%-4d %-30s %-10s %-20s\n",
                i + 1,
                scripts[i].name,
                (scripts[i].permissions & S_IXUSR) ? "Yes" : "No",
                timestamp);
        }
        printf("%-4d %s\n", scriptCount + 1, "Back to previous menu");
}

int executeScript(const Script* script) {
    if (!script) {
        return SCRIPT_PATH_ERROR;
    }

    // Verify file still exists and has same permissions
    struct stat st;
    if (stat(script->path, &st) != 0) {
        return SCRIPT_PATH_ERROR;
    }

    // Check if file was modified since we listed it
    if (st.st_mtime != script->modified) {
        return SCRIPT_PERMISSION_ERROR;
    }

    // Get file extension
    const char *extension = strrchr(script->path, '.');
    if (!extension) {
        return SCRIPT_PATH_ERROR;
    }

    // Special handling for C files
    if (strcasecmp(extension, ".c") == 0) {
        return compile_and_execute_c(script->path);
    }

    // Get appropriate shell
    const char *shell = getScriptShell(script->path, extension);
    if (!shell) {
        return SCRIPT_EXECUTION_ERROR;
    }

    pid_t pid = fork();
    if (pid == -1) {
        if (shell) free((void*)shell);
        return SCRIPT_EXECUTION_ERROR;
    }

    if (pid == 0) {  // Child process
        // Set up a clean environment
        char *const envp[] = {
            "PATH=/usr/local/bin:/usr/bin:/bin",
            NULL
        };

        execle(shell, shell, script->path, NULL, envp);
        if (shell) free((void*)shell);
        _exit(127);  // execle failed
    }

    // Parent process
    if (shell) free((void*)shell);

    int status;
    if (waitpid(pid, &status, 0) == -1) {
        return SCRIPT_EXECUTION_ERROR;
    }

    return WIFEXITED(status) ? WEXITSTATUS(status) : SCRIPT_EXECUTION_ERROR;
}

void setConfigFile(const char* path) {
    current_config_file = path;
}

const char* getConfigFile(void) {
    return current_config_file;
}

bool hasConfigFile(void) {
    return current_config_file != NULL;
}

void display_special_day() {
    time_t t = time(NULL);
    struct tm *current_time = localtime(&t);
    int month = current_time->tm_mon + 1;    // tm_mon is 0-11, so add 1
    int day = current_time->tm_mday;

    if (month == 12 && (day >= 24 && day <= 26)) {
        printf("Merry Christmas!\n");
        printf("We wish you a nice holiday and hope you enjoy our program.\n");
        printf("\n");
    } else if (month == 1 && day == 1) {
        printf("Happy New Year!\n");
        printf("We wish you a nice holiday and hope you enjoy our program.\n");
        printf("\n");
    } else if (month == 12 && day == 31) {
        printf("Happy New Year's Eve!\n");
        printf("We wish you a nice holiday and hope you enjoy our program.\n");
        printf("\n");
    }
}

int create_dir(const char* directory) {
    if (directory == NULL) {
        fprintf(stderr, "Result Error: Directory path is NULL.\n");
        return -1;
    }

    // Resolve the directory path (expand tilde if needed)
    char resolved_dir[PATH_MAX];
    if (directory[0] == '~') {
        const char* home = getenv("HOME");
        if (home == NULL) {
            fprintf(stderr, "Result Error: HOME environment variable is not set.\n");
            return -1;
        }
        // If directory starts with "~/" (or just "~"), combine HOME with the rest of the path.
        if (directory[1] == '/' || directory[1] == '\0') {
            snprintf(resolved_dir, sizeof(resolved_dir), "%s%s", home, directory + 1);
        } else {
            // Tilde expansion for usernames is not supported in this function.
            fprintf(stderr, "Result Error: Tilde expansion for usernames is not supported: %s\n", directory);
            return -1;
        }
    } else {
        // Use directory as provided.
        snprintf(resolved_dir, sizeof(resolved_dir), "%s", directory);
    }

    // Check if the directory already exists.
    struct stat st;
    if (stat(resolved_dir, &st) == 0) {
        if (S_ISDIR(st.st_mode)) {
            printf("Result: Directory already exists: %s\n", resolved_dir);
            return 0;
        } else {
            fprintf(stderr, "Result Error: A file with the same name exists: %s\n", resolved_dir);
            return -1;
        }
    }

    // Construct command to create the directory.
    char command[PATH_MAX * 2];
    int n = snprintf(command, sizeof(command), "mkdir -p \"%s\"", resolved_dir);
    if (n < 0 || n >= (int)sizeof(command)) {
        fprintf(stderr, "Result Error: Path construction failed for %s\n", resolved_dir);
        return -1;
    }

    // Create the directory.
    int ret = system(command);
    if (ret == -1) {
        perror("Result Error: system() call failed");
        return -1;
    } else if (ret != 0) {
        fprintf(stderr, "Result Error: mkdir command returned non-zero status: %d\n", ret);
        return ret;
    } else {
        printf("Result: Directory created successfully: %s\n", resolved_dir);
        return 0;
    }
}

int scan_arch(void) {
    // Call the runCommand function with "uname -m" to scan the architecture
    return runCommand("uname -m");
}

static char *trimWhitespace(char *str) {
    char *end;
    // Trim leading space
    while (isspace((unsigned char)*str)) {
        str++;
    }
    if (*str == '\0') {  // All spaces?
        return str;
    }
    // Trim trailing space
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end)) {
        end--;
    }
    *(end + 1) = '\0';  // Write new null terminator
    return str;
}

// The program_version function accepts a comma-separated string
// of program names, splits it into tokens, and outputs the version of each.
void program_version(const char *programsStr) {
    if (programsStr == NULL) {
        printf("No programs provided.\n");
        return;
    }

    // Duplicate the input string since strtok modifies it.
    char *programs = strdup(programsStr);
    if (programs == NULL) {
        fprintf(stderr, "Memory allocation error.\n");
        return;
    }

    // Split the string on commas.
    char *token = strtok(programs, ",");
    while (token != NULL) {
        // Trim whitespace from the token.
        char *trimmed = trimWhitespace(token);
        if (strlen(trimmed) > 0) {
            // Get the version info using the getVersion function.
            char *version = getVersion(trimmed);
            if (version) {
                printf("%s\n\n", version);
                free(version);
            } else {
                printf("Unable to retrieve version for %s\n\n", trimmed);
            }
        }
        token = strtok(NULL, ",");
    }

    free(programs);
}

char* get_real_user(void) {
    char *real_user = NULL;
    struct utmp *ut;
    setutent();  // Open utmp file
    
    while ((ut = getutent()) != NULL) {
        if (ut->ut_type == USER_PROCESS) {  // Found a user process
            real_user = malloc(UT_NAMESIZE + 1);  // Allocate space for the username
            if (real_user) {
                strncpy(real_user, ut->ut_user, UT_NAMESIZE);  // Copy the username
                real_user[UT_NAMESIZE] = '\0';  // Ensure null termination
            }
            break;
        }
    }
    
    endutent();  // Close utmp file
    return real_user ? real_user : strdup("unknown");  // Return username or "unknown" if not found
}

const char* get_arch_string(void) {
    FILE *fp;
    char arch[64];

    fp = popen("uname -m", "r");
    if (fp == NULL) {
        return "unknown";
    }

    if (fgets(arch, sizeof(arch), fp) != NULL) {
        char *newline = strchr(arch, '\n');
        if (newline) {
            *newline = '\0';
        }

        pclose(fp);
        
        if (strcmp(arch, "x86_64") == 0) {
            return "x86_64";
        } else if (strcmp(arch, "aarch64") == 0) {
            return "aarch64";
        }
    }
    
    pclose(fp);
    return "unknown";
}

const char* get_ostree_variant(void) {
    FILE *file = fopen("/etc/os-release", "r");
    if (file == NULL) {
        return "unknown";
    }

    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (strncmp(line, "VARIANT_ID=", 11) == 0) {
            char *value = line + 11;
            if (value[0] == '"') {
                value++;
                char *quote = strchr(value, '"');
                if (quote) *quote = '\0';
            }
            
            char *newline = strchr(value, '\n');
            if (newline) *newline = '\0';

            
            if (strcmp(value, "silverblue") == 0) {
                return "silverblue";
            } else if (strcmp(value, "kinoite") == 0) {
                return "kinoite";
            } else if (strcmp(value, "sway-atomic") == 0) {
                return "sericea";
            } else if (strcmp(value, "budgie-atomic") == 0) {
                return "onyx";
            } else if (strcmp(value, "base") == 0) {
                return "base";
            }
        }
    }

    fclose(file);
    return "unknown";
}