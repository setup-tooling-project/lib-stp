#ifndef LIBSTP_C_H
#define LIBSTP_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h> // For bool type
#include <stdarg.h> // For va_list
#include <sys/stat.h>
#include <dirent.h>

#define PROGRAMS_PATH "/usr/bin/"

#define CLI 0
#define TUI 1
#define GUI 2
#define Menu 3

#define MAX_SCRIPTS 128
#define MAX_PATH PATH_MAX
#define MAX_NAME NAME_MAX

typedef struct {
    char name[MAX_NAME];
    char path[MAX_PATH];
    mode_t permissions;  // Store file permissions
    time_t modified;
} Script;

enum ScriptError {
    SCRIPT_SUCCESS = 0,
    SCRIPT_DIR_ERROR = -1,
    SCRIPT_PERMISSION_ERROR = -2,
    SCRIPT_PATH_ERROR = -3,
    SCRIPT_BOUNDS_ERROR = -4,
    SCRIPT_EXECUTION_ERROR = -5,
    CLOUD_CONFIG_ERROR = -8,
    VM_DETECTION_ERROR = -9
};

// Function declarations
char *strlwr(char *str);
int runCommand(const char* format, ...);
bool commandExists(const char* command);
bool installProgram(const char *command);
int getMenuOption(int maxOption);
void extractOSVersion(char *version);
void extractLinuxDistro(char *distro);
bool installWithPackageManager(const char* packageName, const char* packageManager);
bool uninstallWithFlatpak(const char* packageName);
bool uninstallWithDNF(const char* packageName);
bool uninstallWithRO(const char* packageName);
bool uninstallWithDNF5(const char* packageName);
bool uninstallWithSnap(const char* packageName);
bool searchWithFlatpak(const char* packageName);
bool searchWithDNF(const char* packageName);
bool searchWithRO(const char* packageName);
bool searchWithDNF5(const char* packageName);
bool searchWithSnap(const char* packageName);
bool isValidPackageName(const char *packageName);
int checkProgramExists(const char* programName);
int shouldShowNotice_tp();
int shouldShowNotice_beta();
void autostart_beta();
void saveUserResponse_tp(char response);
char* getSecurePasswordSTP();
char* getVersion(const char* programName);
void set_log_path();
const char* getScriptShell(const char* path, const char* extension);
int compile_and_execute_c(const char* source_path);
int listScripts(const char* directory, Script scripts[], int current_count, int max_scripts);
void displayScriptMenu(Script scripts[], int scriptCount);
int executeScript(const Script* script);
void setConfigFile(const char* path);
const char* getConfigFile(void);
bool hasConfigFile(void);
void display_special_day();
int create_dir(const char* directory);
int scan_arch(void);
void program_version(const char *programsStr);
char* get_real_user(void);
const char* get_arch_string(void);
const char* get_ostree_variant(void);

#ifdef __cplusplus
}
#endif

#endif
