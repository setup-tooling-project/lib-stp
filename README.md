## libstp

# What is libstp?

- libstp is the library used by every "Setup-Tool" version from version 1.7.0 forward.
- It includes functions needed for the Setup-Tool to work.


# supported architectures

- aarch64
- x86_64

# languages

- C