1.1.3: RELEASED
- [x] fixed [ISSUE#1](https://gitlab.com/setup-tooling-project/libstp/-/issues/1)

1.2.0: RELEASED
- [x] support for more script types
- [x] support for C programs in 'int listScripts(const char* directory, Script scripts[], int current_count, int max_scripts)'
- [x] --> supported libraries: standard libs and libstp
- [x] --> only .c files required (NO BINARIES!!!)

1.2.1: RELEASED
- [x] holiday notices for Christmas and New Year

1.2.2: RELEASED
- [x] new functions

2025.02.10: RELEASED
- [x] new functions
- [x] versioning change (from x.y.z to YEAR.MONTH.DAY)

2025.02.15: RELEASED
- [x] new functions
- [x] changes to flatpak related functions

2025.02.23: RELEASED
- [x] new functions for rpm-ostree

2025.02.26: RELEASED
- [x] bug fixes

2025.02.28: RELEASED (Stable)
- [x] bug fixes